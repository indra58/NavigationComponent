package com.example.endra.testapp;


import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;


/**
 * A simple {@link Fragment} subclass.
 */
public class LoginFragment extends Fragment {

    TextInputEditText etUserName;
    TextInputEditText etPassword;

    View view;

    public LoginFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_login, container, false);

        etUserName = view.findViewById(R.id.etUsername);
        etPassword = view.findViewById(R.id.etPassword);

        Bundle bundle = getArguments();

        if(bundle != null){
            etUserName.setText(bundle.getString("username"));
            etPassword.setText(bundle.getString("password"));
        }

        view.findViewById(R.id.btnLogin).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(etUserName.getText().toString().isEmpty()){
                    Toast.makeText(getContext(), "Provide Username", Toast.LENGTH_SHORT).show();
                    etUserName.requestFocus();
                    return;
                }

                Bundle bundle = new Bundle();
                bundle.putString("username", etUserName.getText().toString());

                Navigation.findNavController(view).navigate(R.id.toMainFragment, bundle);
            }
        });

        return view;
    }

}
