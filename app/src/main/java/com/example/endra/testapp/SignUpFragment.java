package com.example.endra.testapp;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;
import androidx.navigation.ui.NavigationUI;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import org.w3c.dom.Text;


public class SignUpFragment extends Fragment {

    View view;

    TextInputEditText etUserName;
    TextInputEditText etPassword;

    TextInputLayout usernameWrapper;
    TextInputLayout passwordWrapper;

    public SignUpFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_register, container, false);

        usernameWrapper = view.findViewById(R.id.usernameWrapper);
        passwordWrapper = view.findViewById(R.id.passwordWrapper);

        etUserName = view.findViewById(R.id.etUsername);
        etPassword = view.findViewById(R.id.etPassword);

        etUserName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(charSequence.length() > 0){
                    usernameWrapper.setError(null);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        etPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(charSequence.length() > 0){
                    passwordWrapper.setError(null);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        view.findViewById(R.id.btnSignUp).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (etUserName.getText().toString().isEmpty()) {
                    usernameWrapper.setError("Username is Required");
                    etUserName.requestFocus();
                    return;
                }

                if (etPassword.getText().toString().isEmpty()) {
                    passwordWrapper.setError("Password is required");
                    etPassword.requestFocus();
                    return;
                }

                if (etUserName.getText().toString().equals("test")
                        && etPassword.getText().toString().equals("test")) {
                    Bundle bundle = new Bundle();
                    bundle.putString("username", etUserName.getText().toString());
                    bundle.putString("password", etPassword.getText().toString());

                    Navigation.findNavController(view).navigate(R.id.toLogin, bundle);
                } else {
                    Toast.makeText(getContext(), "Provide Valid Details", Toast.LENGTH_SHORT).show();
                }
            }
        });

        return view;
    }
}
